package com.descuplaspt.app;

import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

final class Rotation {
    private static final int ROTATION_DURATION = 750;
    private static final StopWatch STOPWATCH = new StopWatch();
    private static boolean clockwise = true;

    static void rotate(ImageView imageView) {
        long elapsedTime = STOPWATCH.getElapsedTime();
        if ((elapsedTime == 0) || (elapsedTime > ROTATION_DURATION)) {
            float degree;
            if (clockwise) {
                degree = 360f;
            } else {
                degree = -360f;
            }
            RotateAnimation anim = new RotateAnimation(0f, degree,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(ROTATION_DURATION);
            imageView.startAnimation(anim);

            STOPWATCH.start();
            clockwise = !clockwise;
        }
    }
}
