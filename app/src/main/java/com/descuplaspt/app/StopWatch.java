package com.descuplaspt.app;

final class StopWatch {
    private long startTime = 0;

    void start() {
        this.startTime = System.currentTimeMillis();
    }

    long getElapsedTime() {
        return System.currentTimeMillis() - startTime;
    }
}