package com.descuplaspt.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

final class Excuses {
    private static final List<String> problem = new ArrayList<>();
    private static final List<String> culprit = new ArrayList<>();
    private static final List<String> nature = new ArrayList<>();
    private static final List<String> reason = new ArrayList<>();
    private static final List<String> victim = new ArrayList<>();

    // 369.600 possibilities of excuses: 5/3/2016
    static {
        // 10
        problem.add("A alta do dólar é ");
        problem.add("A alta rejeição do governo é ");
        problem.add("A crise é ");
        problem.add("A operação lava-jato é ");
        problem.add("As denúncias premiadas são ");
        problem.add("As manifestações são ");
        problem.add("As vaias são ");
        problem.add("O impeachment é ");
        problem.add("O panelaço é ");
        problem.add("Os ataques à Petrobras são ");

        // 12
        nature.add("um ato de ódio ");
        nature.add("um complô ");
        nature.add("um golpe ");
        nature.add("um truque ");
        nature.add("uma bandeira ");
        nature.add("uma estratégia ");
        nature.add("uma falácia ");
        nature.add("uma intriga ");
        nature.add("uma invenção ");
        nature.add("uma manobra ");
        nature.add("uma teoria ");
        nature.add("uma tática ");

        // 20
        culprit.add("da direita fascista ");
        culprit.add("da elite branca ");
        culprit.add("da imprensa fascista ");
        culprit.add("da mídia golpista ");
        culprit.add("da oposição ");
        culprit.add("da Rede Globo ");
        culprit.add("da VEJA ");
        culprit.add("do capitalismo selvagem ");
        culprit.add("do Danilo Gentili ");
        culprit.add("do FHC ");
        culprit.add("do japonês da federal ");
        culprit.add("do Juiz Sérgio Moro ");
        culprit.add("do Lobão ");
        culprit.add("do Olavo de Carvalho ");
        culprit.add("do PSDB ");
        culprit.add("do TCU ");
        culprit.add("dos almofadinhas ");
        culprit.add("dos burgueses ");
        culprit.add("dos coxinhas ");
        culprit.add("dos Estados Unidos ");

        // 11
        reason.add("para ameaçar ");
        reason.add("para controlar ");
        reason.add("para desmoralizar ");
        reason.add("para desorganizar ");
        reason.add("para desrespeitar ");
        reason.add("para destituir ");
        reason.add("para destruir ");
        reason.add("para manipular ");
        reason.add("para oprimir ");
        reason.add("para subverter ");
        reason.add("para prejudicar ");

        // 14
        victim.add("a mandioca!");
        victim.add("a Mulher-Sapiens!");
        victim.add("a pátria educadora!");
        victim.add("a presidenta!");
        victim.add("o Lula!");
        victim.add("o povo!");
        victim.add("o PT!");
        victim.add("o resultado das urnas!");
        victim.add("as conquistas dos trabalhadores!");
        victim.add("os avanços da democracia!");
        victim.add("os companheiros petistas!");
        victim.add("os direitos dos trabalhadores!");
        victim.add("os movimentos sociais!");
        victim.add("os pobres!");
    }

    static String selectExcuse() {
        String excuse = "";
        Random randomGenerator = new Random();

        excuse += problem.get(randomGenerator.nextInt(problem.size()));
        excuse += nature.get(randomGenerator.nextInt(nature.size()));
        excuse += culprit.get(randomGenerator.nextInt(culprit.size()));
        excuse += reason.get(randomGenerator.nextInt(reason.size()));
        excuse += victim.get(randomGenerator.nextInt(victim.size()));

        return excuse;
    }
}
