package com.descuplaspt.app;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends Activity {

    private static final String TWITTER_KEY = "ieYT6zj2HR2EVlCzIkgSML1yF";
    private static final String TWITTER_SECRET = "dVzFbNF1aEuLJcWvdrMMHJnPFpbzSxuJZW1TvSuUR9erbkCdbH";

    private boolean doubleBackToExitPressedOnce = false;
    private boolean doubleTapToCopyPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        Fabric.with(this, new TweetComposer());

        setContentView(R.layout.activity_main);
        setNewExcuse(null);

        TextView tvInitialMsg = (TextView) findViewById(R.id.textViewInfoInitialMsg);
        Spanned text = Html.fromHtml("Se você é petista ou tem algum amigo petista, esse aplicativo é para você!<br>"
                + "Agora ficou fácil você ganhar qualquer discussão contra os coxinhas!<br>"
                + "Basta clicar na estrela do PT para gerar uma desculpa automática e legítima!");
        tvInitialMsg.setText(text, TextView.BufferType.SPANNABLE);
        Spannable spannable = (Spannable) tvInitialMsg.getText();
        spannable.setSpan(new StrikethroughSpan(), 98, 104, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new StrikethroughSpan(), 211, 219, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        final TextView tvExcuse = (TextView) findViewById(R.id.textViewInfoExcuse);
        tvExcuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (doubleTapToCopyPressedOnce) {
                    copyExcuseToClipboard(tvExcuse.getText());
                    return;
                }
                doubleTapToCopyPressedOnce = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleTapToCopyPressedOnce = false;
                    }
                }, 2000);
            }
        });
        tvExcuse.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                copyExcuseToClipboard(tvExcuse.getText());
                return true;
            }
        });

        // Google ads
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.double_back_to_exit, Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void setNewExcuse(View view) {
        if (view != null) {
            Rotation.rotate((ImageView) findViewById(R.id.excuseButton));
            view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }

        TextView tv = (TextView) findViewById(R.id.textViewInfoExcuse);
        String phrase = Excuses.selectExcuse();
        SpannableString spannable = new SpannableString(phrase);
        spannable.setSpan(new StyleSpan(Typeface.BOLD), 0, spannable.length(), 0);
        tv.setText(spannable);
    }

    public void onShareFacebookClicked(View view) {
        String excuse = ((TextView) findViewById(R.id.textViewInfoExcuse)).getText().toString();

        FacebookSdk.sdkInitialize(getApplicationContext());
        ShareDialog shareDialog = new ShareDialog(this);
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("Desculpas do PT")
                    .setContentDescription(excuse)
                    .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.descuplaspt.app"))
                    .build();
            shareDialog.show(linkContent);
        }
    }

    public void onShareTwitterClicked(View view) {
        String msgToShare = "Gerador de Desculpas do PT geradordedesculpasdopt.com.br #GeradorDeDesculpasDoPT #Android";
        new TweetComposer.Builder(this).text(msgToShare).show();
    }

    public void onShareWhatsAppClicked(View view) {
        String whatsAppPackage = "com.whatsapp";
        if (whatsAppIsInstalled(whatsAppPackage)) {
            String excuse = ((TextView) findViewById(R.id.textViewInfoExcuse)).getText().toString();
            excuse += " #DesculpasDoPT https://play.google.com/store/apps/details?id=com.descuplaspt.app";

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, excuse);
            sendIntent.setType("text/plain");
            sendIntent.setPackage(whatsAppPackage);
            startActivity(sendIntent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + whatsAppPackage)));
        }
    }

    private void copyExcuseToClipboard(CharSequence text) {
        Context context = getApplicationContext();
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("text_label", text);
        cm.setPrimaryClip(clipData);
        Toast.makeText(context, R.string.excuse_copied, Toast.LENGTH_SHORT).show();
    }

    private boolean whatsAppIsInstalled(String whatsAppPackage) {
        try {
            getPackageManager().getPackageInfo(whatsAppPackage, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
